#! /usr/bin/env python
""" letters
"""
from itertools import permutations
import enchant

_letters = []
_potential_words = []
_min_word_size = 4
_must_contain_letter = None

def generate_potential_words():
    """ Method to generate potential words from _letters
    """
    global _potential_words
    for count in range(_min_word_size, len(_letters) + 1):
        result = permutations(_letters, count)
        for item in result:
            _potential_words.append("".join(item))

    print(len(_potential_words))


def check_and_print_valid_words():
    global _potential_words

    if len(_potential_words) == 0:
        print("No words to check")
        return

    counter = 0
    dictionary = enchant.Dict("en_US")
    for word in sorted(set(_potential_words), key=len):
        if dictionary.check(word) and _must_contain_letter in word:
            print(word)
            counter += 1

    print("Total words: ", counter)


def get_input():
    global _letters
    global _min_word_size
    global _must_contain_letter

    letters = raw_input('Enter the letters: ')
    _letters = list(letters)
    _min_word_size = int(raw_input("Enter min word size: "))
    _must_contain_letter = raw_input('Must contain letter: ')


def main():
    """ Main method
    """
    get_input()
    generate_potential_words()
    check_and_print_valid_words()

if __name__ == "__main__":
    main()